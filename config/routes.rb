Rails.application.routes.draw do
  root 'paginas#index'

  get 'paginas/cronograma'

  get 'paginas/inscricoes'

  get 'paginas/galeria'

  get 'paginas/2017'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

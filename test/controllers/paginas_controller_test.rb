require 'test_helper'

class PaginasControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get paginas_index_url
    assert_response :success
  end

  test "should get cronograma" do
    get paginas_cronograma_url
    assert_response :success
  end

  test "should get inscricoes" do
    get paginas_inscricoes_url
    assert_response :success
  end

  test "should get galeria" do
    get paginas_galeria_url
    assert_response :success
  end

  test "should get 2017" do
    get paginas_2017_url
    assert_response :success
  end

end
